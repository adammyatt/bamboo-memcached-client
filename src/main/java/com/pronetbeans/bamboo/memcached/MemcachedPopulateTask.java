package com.pronetbeans.bamboo.memcached;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.*;
import com.danga.MemCached.MemCachedClient;
import com.danga.MemCached.SockIOPool;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.apache.log4j.Logger;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.PatternSet;
import org.jetbrains.annotations.NotNull;

/**
 * Bamboo Task for Memcached Client data population.
 *
 * @author Adam Myatt
 */
public class MemcachedPopulateTask implements TaskType {

    private static final Logger log = Logger.getLogger(MemcachedPopulateTask.class);

    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {

        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.create(taskContext);
        final BuildLogger buildLogger = taskContext.getBuildLogger();

        final String propsServers = taskContext.getConfigurationMap().get("propsServers");
        final String propsInstanceName = taskContext.getConfigurationMap().get("propsInstanceName");
        final String propsValuesFile = taskContext.getConfigurationMap().get("propsValuesFile");

        final File workingDirectory = taskContext.getWorkingDirectory();
        boolean itWorked = false;

        try {






            //initialize the SockIOPool that maintains the Memcached Server Connection Pool
            String[] servers = propsServers.split("[,]");

            SockIOPool pool = SockIOPool.getInstance(propsInstanceName);
            pool.setServers(servers);
            pool.setFailover(true);
            pool.setInitConn(10);
            pool.setMinConn(5);
            pool.setMaxConn(250);
            pool.setMaintSleep(30);
            pool.setNagle(false);
            pool.setSocketTO(3000);
            pool.setAliveCheck(true);
            pool.initialize();

            //Get the Memcached Client from SockIOPool named Test1
            MemCachedClient mcc = new MemCachedClient(propsInstanceName);

            long lStart = System.currentTimeMillis();
            buildLogger.addBuildLogEntry("Population Start");

            FileSet fileSet = new FileSet();
            fileSet.setDir(workingDirectory);

            // exclude the "build-number.txt" fiel that is automatically in each build dir (this is a 
            // file used by Bamboo
            PatternSet.NameEntry exclude = fileSet.createExclude();
            exclude.setName("build-number.txt");

            PatternSet.NameEntry include = fileSet.createInclude();
            include.setName(propsValuesFile);

            DirectoryScanner ds = fileSet.getDirectoryScanner(new Project());
            String[] srcFiles = ds.getIncludedFiles();

            for (String srcFile : srcFiles) {

                BufferedReader br = null;
                try {
                    File input = new File(workingDirectory, srcFile);

                    br = new BufferedReader(new FileReader(input));
                    String line;
                    while ((line = br.readLine()) != null) {

                        final int equalIndex = line.indexOf("=");
                        final String sKey = line.substring(0, equalIndex);
                        final String sValue = line.substring(equalIndex + 1, line.length());

                        buildLogger.addBuildLogEntry("Populating key=value : " + sKey + "=" + sValue);
                        mcc.add(sKey, sValue);
                    }

                } catch (Exception e) {
                    buildLogger.addErrorLogEntry("Error loading key=value properties from file '" + srcFile + "': " + e.getMessage(), e);
                } finally {
                    try {
                        if (br != null) {
                            br.close();
                        }
                    } catch (Exception e) {
                        buildLogger.addErrorLogEntry("Error closing BufferedReader : " + e.getMessage(), e);
                    }
                }
            }

            buildLogger.addBuildLogEntry("Population End , Time : " + (System.currentTimeMillis() - lStart));

            itWorked = true;

            log.info(Utils.getLogBanner());

        } catch (Exception e) {
            buildLogger.addErrorLogEntry("Error : " + e.getMessage(), e);
        } finally {

            try {
            } catch (Exception e) {
                buildLogger.addErrorLogEntry("Error : " + e.getMessage(), e);
            }
        }

        if (itWorked) {
            return TaskResultBuilder.create(taskContext).success().build();
        } else {
            return TaskResultBuilder.create(taskContext).failedWithError().build();
        }
    }
}