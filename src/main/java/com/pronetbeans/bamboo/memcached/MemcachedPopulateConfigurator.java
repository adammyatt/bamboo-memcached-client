package com.pronetbeans.bamboo.memcached;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.opensymphony.xwork.TextProvider;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Class for configuring parameters used by class for zipping files.
 * 
 * @author Adam Myatt
 */
public class MemcachedPopulateConfigurator extends AbstractTaskConfigurator {

    private TextProvider textProvider;

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put("propsServers", params.getString("propsServers"));
        config.put("propsInstanceName", params.getString("propsInstanceName"));
        config.put("propsValuesFile", params.getString("propsValuesFile"));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        context.put("propsServers", taskDefinition.getConfiguration().get("propsServers"));
        context.put("propsInstanceName", taskDefinition.getConfiguration().get("propsInstanceName"));
        context.put("propsValuesFile", taskDefinition.getConfiguration().get("propsValuesFile"));
        
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        context.put("propsServers", taskDefinition.getConfiguration().get("propsServers"));
        context.put("propsInstanceName", taskDefinition.getConfiguration().get("propsInstanceName"));
        context.put("propsValuesFile", taskDefinition.getConfiguration().get("propsValuesFile"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        final String propsServers = params.getString("propsServers");
        if (StringUtils.isEmpty(propsServers)) {
            errorCollection.addError("propsServers", textProvider.getText("com.pronetbeans.bamboo.memcached.MemcachedPopulateTask.error"));
        }
        final String propsInstanceName = params.getString("propsInstanceName");
        if (StringUtils.isEmpty(propsInstanceName)) {
            errorCollection.addError("propsInstanceName", textProvider.getText("com.pronetbeans.bamboo.memcached.MemcachedPopulateTask.error"));
        }
        final String propsValuesFile = params.getString("propsValuesFile");
        if (StringUtils.isEmpty(propsValuesFile)) {
            errorCollection.addError("propsValuesFile", textProvider.getText("com.pronetbeans.bamboo.memcached.MemcachedPopulateTask.error"));
        }        
    }

    public void setTextProvider(final TextProvider textProvider) {
        this.textProvider = textProvider;
    }
}
